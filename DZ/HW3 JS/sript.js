"use strict"
let a;
let b;
let c;
do {
    a = +prompt('Введиьте число 1');
}
while ((!a || a === "" || isNaN(+a)));
do {
    b = +prompt('Введиьте число 2');
}
while ((!b || b === "" || isNaN(+b)));
c = prompt('Введитн знак желаемой операции');

function results() {
    switch (c) {
        case '-':
            return a - b;
        case '+':
            return a + b;
        case '/':
            return a / b;
        case '*':
            return a * b;
        default:
            console.log('Не возможно выполнить действие');
    }
}
console.log(results())