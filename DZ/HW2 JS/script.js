"use strict"

let a;
let m;
let n;
do {
    a = prompt('Введите любое число');
} while (!a || a.trim() === "" || isNaN(+a) || (a % 1 !== 0));

for (let i = 0; i <= a; i++) {
    if (a < 5) {
        console.log('Sorry, no numbers');
        break;
    }
    if (i % 5 === 0)
        console.log(i);
}